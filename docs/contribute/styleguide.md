# Styleguide

## Elixir

We format our code with the Elixir Formatter and check for issues with [Credo](https://github.com/rrrene/credo) (a few rules are not blocking).

Please run these two commands before pushing code:

 * `mix format`
 * `mix credo --strict -a`
 
These two commands must not return an error code, since they are required to pass inside CI.

## Front-end

### Linting

We use `prettier` and `eslint`.
Errors should be reported when the development server is running or when building a production bundle `npm run build`.

Please run the following commands before pushing code `npm run lint` and `npm run prettier --check .`.

This command must not return an error code, since it's required to pass inside CI.

We also try to follow the [official Vue.js style guide](https://vuejs.org/v2/style-guide/).

## Tooling

In order to apply automatic linting before committing any code, run `git config --local core.hooksPath ".husky/_"` locally. This has to be done manually because cloning a repo does not allow to apply config automatically.

The pre-commit hook will run linters and try to fix errors or abort commit and report if they can not fix it by themselves.
