# Global search

Since Mobilizon 3.0.0, a global search feature is available, using the API from https://search.joinmobilizon.org.

Since Mobilizon 5.1.0, this global search has been discontinued and is unavailable in the frontend. It is recommended to keep the default configuration unchanged. See [issue #1545](https://framagit.org/framasoft/mobilizon/-/issues/1545).

## Configuration

The following is the default configuration.
```elixir
config :mobilizon, :search, global: [
    is_default_search: false,
    is_enabled: true
]
```

Setting `is_enabled` to `false` will disable the feature, whereas setting `is_default_search` to `true` will make the global search the default when searching on the instance.