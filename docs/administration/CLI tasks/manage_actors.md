# Manage actors
## List all available commands

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl actors
    ```

=== "Docker"
    ```bash
    docker compose exec mobilizon mobilizon_ctl actors
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.actors
    ```

## Create a new profile

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl actors.new [<options>]
    ```

=== "Docker"
    ```bash
    docker compose exec mobilizon mobilizon_ctl actors.new [<options>]
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.actors.new [<options>]
    ```

### Options

* `--email`/`-e` - Provide the email for the user you want to attach the profile to. Required.
* `--username`/`-u` - The username for the profile. If `--display-name` isn't given at the same time, this value will also be used as display name.
* `--display-name`/`-d` - The displayed name for the profile. If `--username` isn't given at the same time, this value will be transformed to create an acceptable username.
* `--type`/`-t` - The type of actor to create. Defaults to `person`.

## Create a new group

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl actors.new [<options>]
    ```

=== "Docker"
    ```bash
    docker compose exec mobilizon mobilizon_ctl actors.new [<options>]
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.actors.new [<options>]
    ```

### Options

* `--username`/`-u` - The username for the group. If `--display-name` isn't given at the same time, this value will also be used as display name.
* `--display-name`/`-d` - The displayed name for the group. If `--username` isn't given at the same time, this value will be transformed to create an acceptable username.
* `--group-admin`/`-a` - The username of the profile that will be the group admin. Required.
* `--type`/`-t` - The type of actor to create. Defaults to `person`. Set it to `group` to create a group.

## Show an actors details

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl actors.show <actor_username>
    ```

=== "Docker"
    ```bash
    docker compose exec mobilizon mobilizon_ctl actors.show <actor_username>
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.actors.show <actor_username>
    ```

Where `<actor_username>` is a local or federated, profile or group, such as `myprofile`, `mygroup`, `remote_profile@remote_instance.com` or `mygroup@my_remote_instance.com`.

In addition to basic informations, it also tells which user the profile belongs to, if local.

## Refresh a remote actor

Can be helpful in case of federation issues.

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl actors.refresh <federated_group_name>
    ```

=== "Docker"
    ```bash
    docker compose exec mobilizon mobilizon_ctl actors.refresh <federated_group_name>
    ```

=== "Source"
    ```bash
    MIX_ENV=prod mix mobilizon.actors.refresh <federated_group_name>
    ```

Where `<federated_group_name>` is a federated group name like `mygroup@my_remote_instance.com`.